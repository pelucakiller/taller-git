# Guia básica de GIT

### README.md
Contiene la descripción del proyecto, prerequisitos, pasos de instalacion, como ejecutar tests, herramientas, autores, herramientas de versionado, etc...
Para dar formato al README, como su extensión lo indica, se utiliza markdown.  

## Markdown  
Agregar formato mediante markdown es muy facil.  

#### Saltos de linea
Los saltos de linea para el texto común.  
  
#### Headers
Se pueden definir con hashtags o con "subrayados"  
Con un # se define el texto como un H1 en HTML, ## H2, y así...  
También se puede hacer un H1 subrayandolo con "====" y un h2 subrayandolo con guiones "-------"  
  
  
#### Énfasis
**Negrita:** Se coloca un texto en negrita con dos asteriscos al inicio y al final de la frase/palabra/letra.  
También se puede conseguir el mismo resultado utilizando guiones bajos en lugar de asteríscos.  
  
**Italic:** Para que un texto esté en italic, se utiliza un asterisco o guion bajo al inicio y final del texto a dar énfasis.  
  
  
#### Listas
**Ordenadas**   
1. Primer elemento  (1. Primer elemento)  
2. Segundo elemento (2. Segundo elemento)  
3. Tercer elemento  (3. Tercer elemento)  

**Unordered**   
Para hacer listas no ordenadas se puede utilizar tanto * + o -  
* Elemento 1   
* Elemento 2   
* Elemento 3   
* etc  

#### Tablas
Header 1 | Header 2
----------|-----------
Dato 1 | Dato 2
Otro   | Sigue siendo una prueba
  
  
  
**Other links:**   
A template to make a good README.md:  
https://gist.github.com/PurpleBooth/109311bb0361f32d87a2  
  
Que es Markdown  
https://hipertextual.com/archivo/2013/04/que-es-markdown/  
  
Como utilizar Markdown  
https://geekytheory.com/que-es-markdown-y-como-utilizarlo  


